using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TDPlayerController : MonoBehaviour
{

    [SerializeField] private GameObject projectile;
    [SerializeField] private Transform bulletDirection;
    [SerializeField] private float bulletDelay = 0.5f;
    [SerializeField] private float movementVelocity = 3f;

    private TDActions controls;
    private Camera cam;
    private bool canShoot = true;
    
    private Vector2 mouseScreenPosition;
    private Vector3 mouseWorldPosition;
    private Vector3 targetDirection;
    private float angle;

    private bool isShooting;

    private void Awake()
    {
        controls = new TDActions();
        cam = Camera.main;
    }

    private void OnEnable()
    {
        controls.Enable();
    }

    void Start()
    {
        controls.Player.Shoot.performed += _ => isShooting = true;
        controls.Player.Shoot.canceled += _ => isShooting = false;
    }

    void Update()
    {
        // Shooting
        if (isShooting) PlayerShoot();
        
        // Rotation
        mouseScreenPosition = controls.Player.MousePosition.ReadValue<Vector2>();
        mouseWorldPosition = cam.ScreenToWorldPoint(mouseScreenPosition);
        targetDirection = mouseWorldPosition - transform.position;
        angle = Mathf.Atan2(targetDirection.y, targetDirection.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, angle));

        // Movement
        Vector3 movement = controls.Player.Movement.ReadValue<Vector2>() * movementVelocity;
        transform.position += movement * Time.deltaTime;
    }
    
    private void PlayerShoot()
    {
        if (!canShoot) return;
        
        Vector2 mousePosition = controls.Player.MousePosition.ReadValue<Vector2>();
        mousePosition = cam.ScreenToWorldPoint(mousePosition);
        GameObject g = Instantiate(projectile, bulletDirection.position, bulletDirection.rotation);
        g.SetActive(true);

        StartCoroutine(CanShoot());
    }

    IEnumerator CanShoot()
    {
        canShoot = false;
        yield return new WaitForSeconds(bulletDelay);
        canShoot = true;
    }
    
    private void OnDisable()
    {
        controls.Disable();
    }
}
