using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{

    [SerializeField] private float speed = 5f;
    [SerializeField] private float timeToLive = 3f;
    
    void Update()
    {
        transform.Translate( speed * Time.deltaTime * Vector3.up);
    }
    
    IEnumerator DestroyBulletAfterTime()
    {
        yield return new WaitForSeconds(timeToLive);
        Destroy(gameObject);
    }
    
    private void OnEnable()
    {
        StartCoroutine(DestroyBulletAfterTime());
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player"))
            Destroy(gameObject);
    }
}
